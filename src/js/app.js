"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import HystModal from 'hystmodal';

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');


// Modal
const myModal = new HystModal({
    linkAttributeName: 'data-hystmodal',
    catchFocus: false
});
